

/* маскирует нужные поля*/

$(document).ready(function(){
	$('.date-mask').mask('99.99.9999',{placeholder:" "});
	$('#passport_code').mask('999-999',{placeholder:" "});
	$('#passport_num').mask('9999 999999',{placeholder:" "});
});




/* переключение между шагами */

$('a.button').click(function(event){
	event.preventDefault();

	var action = $(this).data('action');

	if (action == 'step2') {
		$(this).closest('.step').removeClass('step--active');
		$(this).closest('.step').next().addClass('step--active');
	}

	else if (action == 'step1') {
		$(this).closest('.step').removeClass('step--active');
		$(this).closest('.step').prev().addClass('step--active');
	}


});




/* очищает значение поля от маски */

function getClearInputValue(input) {
	var value = (input.data('rawMaskFn')) ? input.mask() : input.val(); 
	return value;
}





/* разблокирует/блокирует кнопку «Далее» в зависимости от заполненности полей */

function checkEmptyFields() {
	var empty = 0;

	$('.step--active .input__field.required').each(function(i,item){
		if (!getClearInputValue($(item))) {
			empty++;
		}
	});

	if ( $('.step--active .input--error').length > 0 || empty > 0 || $('.step[data-step="1"] .file:not(.file--added)').length > 0 ) {
		$('.step--active .button:not([data-action="step1"]').addClass('button--disabled');
	}

	else {
		$('.step--active .button:not([data-action="step1"]').removeClass('button--disabled');
	}
}

/* выводит ошибку валидации в поле */

function addErrorToField(block, error) {
	block.addClass('input--error');
	block.find('.input__error').html(error);
	block.removeClass('input--ok');
}


/* выводит успех валидации у поля */

function addSuccessToField(block) {
	block.removeClass('input--error');
	block.addClass('input--ok');
}


/* проверяет дату рождения на совершеннолетие */

function checkBirthDate(value) {
	return moment(value,'DD.MM.YYYY').unix() < moment().subtract(18, 'years').unix();
}



/* проверяет имя на небуквенные значения */

function checkName(value) {
	return /^[a-zA-Zа-яА-Я\s]+$/.test(value);
}


/* проверяет email */

function checkEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}



/* проверяет дату выдачи паспорта */

function checkPassportGivenDate(value) {
	var birth = getClearInputValue($('#birth_date'));

	if (birth) {
		var date = moment(value,'DD.MM.YYYY').unix();
		return ( date > moment($('#birth_date').val(),'DD.MM.YYYY').add(14, 'years').unix() ) && ( date < moment().unix() );
	}

	else {
		return true;
	}
	
}


/* разрешает вводить в поле «Стоимость аренды» только цифры */

$('#price').on('keypress', function (event) {
    if (!/^\d+$/.test(event.key)) {
		event.preventDefault();
    }
});


/* считает цену */

function calculatePrice(value) {
	if (value >= 45000) {
		return round10down(654 + (value * 1.5 * 0.05) / 11 + value * 0.0097);
	}

	else {
		return round10up(454 + (value * 1.5 * 0.05) / 11 + value * 0.0097);
	}
}

/* запускает счет цены */

$('#price').on('input',function(event){
	var value = parseInt(event.target.value);
	var block = $(this).closest('.input');
	if (event.target.value.length >=5 ) {
		$('#calculate').text(calculatePrice(value));
		$('.step__price').addClass('step__price--show');
	}

	else {
		$('.step__price').removeClass('step__price--show');
	}

	if (value > 100000) {
		addErrorToField(block, 'Мы работаем с квартирами до 100 000₽/мес.<br/>Напишите на <a href="mailto:help@pik-arenda.ru">help@pik-arenda.ru</a> и мы найдем решение');
		$('.step__price').removeClass('step__price--show');
	}

	else if (value < 20000) {
		addErrorToField(block, 'Мы работаем с квартирами от 20 000₽/мес.<br/>Напишите на <a href="mailto:help@pik-arenda.ru">help@pik-arenda.ru</a> и мы найдем решение');
		$('.step__price').removeClass('step__price--show');
	}

	else {
		block.removeClass('input--error');
	}

});


/* проверяем поле даты выдачи паспорта на случай, если оно было заполнено до даты рождения */

$('#birth_date').on('focusout',function(){
	var passportGivenDateFilled = getClearInputValue($('#passport_given_date'));
	var block = $('#passport_given_date').closest('.input');
	var passportGivenDate = $('#passport_given_date').val();

	if (passportGivenDateFilled && !checkPassportGivenDate(passportGivenDate)) {
		addErrorToField(block,'некорректная дата выдачи');
		checkEmptyFields();
	}
});


/* проверяет поле */

$('.input__field').on('focusout',function(){
	var value = getClearInputValue($(this));
	var required = $(this).hasClass('required');
	var validate = $(this).hasClass('validate');
	var block = $(this).closest('.input');
	var name = $(this).attr('name');

	if (required && value.length === 0) {
		addErrorToField(block, 'это поле нужно заполнить')
	}

	else if (validate) {
		if (name == 'name' && !checkName(value) ) {
			addErrorToField(block, 'поле должно содержать только буквы');
		}

		else if (name == 'birth_date' && !checkBirthDate(value)) {
			addErrorToField(block,'нельзя подписать договор, если вам меньше 18 лет');
		}

		else if (name == 'email' && !checkEmail(value)) {
			addErrorToField(block,'это не похоже на email');
		}

		else if (name == 'passport_given_date' && !checkPassportGivenDate(value)) {
			addErrorToField(block,'некорректная дата выдачи');
		}

		else if (name == 'price' && ( parseInt(value) > 100000 || parseInt(value) < 20000 ) ) {
			
		}

		else {
			addSuccessToField(block)
		}
	}

	else {
		addSuccessToField(block);
	}

	checkEmptyFields();

});



/* подключает dadata */

$("#name").suggestions({
	token: "3e1d895716c77bf999c4f25566689124578f3c6e",
	type: "NAME",
	count: 5,
	/* Вызывается, когда пользователь выбирает одну из подсказок */
	onSelect: function(suggestion) {
		console.log(suggestion);
	}
});
$("#address").suggestions({
	token: "3e1d895716c77bf999c4f25566689124578f3c6e",
	type: "ADDRESS",
	count: 5,
	/* Вызывается, когда пользователь выбирает одну из подсказок */
	onSelect: function(suggestion) {
		console.log(suggestion);
	}
});




/* конфигурирует загрузку файлов */

$(document).ready(function(){
	$('.file').each(function(i,item){
		$(item).dropzone({ 
			url:'#', 
			autoProcessQueue: false, 
			paramName: $(item).attr('id'),
			acceptedFiles: 'image/jpeg,image/png,application/pdf',
			thumbnail: function(file, dataUrl) {
				$(item).attr('data-name',$(item).find('.file__name').text());
			    $(item).find('.file__name').text(file.name);
			    $(item).addClass('file--added');

			    checkEmptyFields();
			},
		});
	});
	
});




/* очищает поле загрузки файла */

$('.file__delete').click(function(){
	var file = $(this).closest('.file');
	Dropzone.forElement('#'+file.attr('id')).removeAllFiles();
	file.removeClass('file--added');
	file.find('.file__name').text(file.data('name'));

	checkEmptyFields();
});



/* отправляет форму */


$('form').on('submit',function(event){
	event.preventDefault();
	var form = $(this).serializeArray();
	$(this).find('.file.file--added').each(function(i, item){
		form.push({
			name: $(item).attr('id'),
			value: Dropzone.forElement('#'+$(item).attr('id')).files
		});
	});

	console.log(form);
});


/* округляет до десятков в большую сторону */

function round10up(val) {
  return Math.round(val / 10) * 10;
}



/* округляет до десятков в меньшую сторону */

function round10down(val) {
  return Math.floor(val / 10) * 10;
}
